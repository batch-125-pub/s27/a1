let http = require("http");
let PORT = 3000;

http.createServer((req, res)=>{
	if(req.url == "/login"){
		//console.log(`page found`);
		res.writeHead(200,
			{"Content-Type":"text/html"}
		);
		res.write("Welcome to the login page");
		res.end();
	} else {
		//console.log(`pge cnnot be found`);
		res.writeHead(400, 
			{"Content-Type": "text/plain"}
		);
		res.write("I'm sorry the page you are looking for cannot be found.");
		res.end();
	}
	
}).listen(PORT);

console.log(`Server is successfully running`);