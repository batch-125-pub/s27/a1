//Questions
//a. What directive is used by Node.js in loading the modules it needs?
//Ans: require

//b. What Node.js module contains a method for server creation?
//Ans: require("http");

//c.What is the method of the http object responsible for creating a server using Node.js?
//Ans: createServer();

//d. What method of the response object allow us to set status codes and content types?
//Ans: writeHead();

//e. Where will console.log() output its contents when run in Node.js?
//Ans. Terminal

//f. What property of the request object contains the address point?
//Ans. url

